# cd to remote folder
mkdir SCDojo_2016_03_11 SCDojo_2016_03_11/Remote SCDojo_2016_03_11/Local
cd SCDojo_2016_03_11/Remote
git init --bare

# cd to local folder
cd ../Local
rm -rf .git; \
git init; \
git remote add origin "https://yoshgoodman@bitbucket.org/gogogit/gogogit.git"; \
touch FirstCommit; git add .; git commit -m "First Commit"; \
touch SecondCommit; git add .; git commit -m "Second Commit"; \
touch ThirdCommit; git add .; git commit -m "Third Commit"; \
touch FourthCommit; git add .; git commit -m "Fourth Commit"; \
touch FifthCommit; git add .; git commit -m "Fifth Commit"; \
touch SixthCommit; git add .; git commit -m "Sixth Commit"; \
git checkout HEAD~3; \
git branch TempBranch; \
git checkout -b FirstBranch; \
touch FirstBranchCommit1; git add .; git commit -m "First Branch - Commit 1"; \
touch FirstBranchCommit2; git add .; git commit -m "First Branch - Commit 2"; \
touch FirstBranchCommit3; git add .; git commit -m "First Branch - Commit 3"; \
git checkout master; \
git checkout -b SecondBranch; \
touch SecondBranchCommit1; git add .; git commit -m "Second Branch - Commit 1"; \
touch SecondBranchCommit2; git add .; git commit -m "Second Branch - Commit 2"; \
touch SecondBranchCommit3; git add .; git commit -m "Second Branch - Commit 3"; \
git checkout master; \
git checkout -b ThirdBranch; \
touch ThirdBranchCommit1; git add .; git commit -m "Third Branch - Commit 1"; \
touch ThirdBranchCommit2; git add .; git commit -m "Third Branch - Commit 2"; \
touch ThirdBranchCommit3; git add .; git commit -m "Third Branch - Commit 3"; \
git branch -D TempBranch; \
git checkout master; \
git log --oneline --abbrev-commit --all --graph --decorate --color;